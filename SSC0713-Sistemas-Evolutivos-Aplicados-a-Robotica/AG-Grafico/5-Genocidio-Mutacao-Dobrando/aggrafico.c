#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <GL/glut.h>
#include <string.h>

#define maxx 1000
#define TamPop 10
float TaxMut = 2;   // 10 = 10%

double ind[TamPop + 1];
double fit[TamPop + 1];
double maxfit = 0.0;
int    i, maxi = 0;
int gen = 0;

// Output
FILE *file;

// Mutacao
double bestInd[5];// Melhor individuo das últimas 5 gerações

//--------------- Functions --------------//
void initpop(void);
void ag(void);
void avalia(void);
void aumentarMutacao(void);
void elitismo(void);
void genocidio(void);
void draw(void);
void drawTextToScreen(void);
void keyboard(unsigned char key, int x, int y);
void saveToTxt(void);

//----------------- Main -----------------//
int main(int argc, char *argv[])
{
	srand(time(NULL));
	file = fopen("data.txt", "w+");

	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(glutGet(GLUT_SCREEN_WIDTH), glutGet(GLUT_SCREEN_HEIGHT));
    glutCreateWindow("Evolutive Systems - Genocidio");

    initpop();

    glutDisplayFunc(draw);
    glutKeyboardFunc(keyboard);
    gluOrtho2D(0, 1000, 0, 1000);
	glutMainLoop();

	fclose(file);
	return 0;
}

void initpop(void)
{
    for (i=1;i<=TamPop;i++)
    {                         // nr entre -10 e +10 -> rand() %20 - 10;
        ind[i] = (double) (rand() % maxx);
    }
}

//just explain the commands
void ag(void)
{
    elitismo();    // Seleciona o melhor individuo para transar com todos e Faz Crossover e Mutacao e ja' substitui na Populacao
	aumentarMutacao();// Aumenta mutacao e faz genocidio se a mutacao ficar maior que 100%
    avalia();       // Avalia os individuos da Populacao

    // Final do Algoritmo Evolutivo
	gen++;
}

void aumentarMutacao(void)
{
	// Change mutation
	if(bestInd[4]-bestInd[0]<0.00001)
		TaxMut = TaxMut*1.2;
	else
		TaxMut = 2;

	if(TaxMut>100)
	{
		genocidio();
		TaxMut = 2;
	}
}

void elitismo(void) // Melhor transa com todos
{
    maxfit = fit[1];
    maxi   = 1;

    for (i=2;i<=TamPop;i++)  // Busca pelo melhor individuo
    {
        if (fit[i]>maxfit)
        {
            maxfit = fit[i];
            maxi = i;
        }
    }

	// Guarda ultimos melhores individuos
	for (i=1;i<5;i++)
	{
		bestInd[i-1] = bestInd[i];
	}
	bestInd[4] = maxfit;

    for (i=1;i<=TamPop;i++)
    {
        if (i==maxi)        // Protege o melhor individuo
            continue;

        // Crossover
        ind[i] = (ind[i] + ind[maxi])/ 2.0;

        // Mutacao                    | nr = 0-40 |    - 20    |  0,02  |  10%
		// Inicialmente era: (rand()%maxx)-maxx/2)*TaxMut/100.0f;
		// Diminui o efeito para o genocídio fazer mais efeito
        ind[i] = ind[i] + ((double) (rand()%maxx/3)-maxx/6)*TaxMut/100.0f;
		if(ind[i]>maxx)
			ind[i]-=maxx;
		if(ind[i]<0)
			ind[i]+=maxx;
    }
}

void genocidio(void)
{
	int indexMelhor = 1;
	int fitnessMelhor = fit[1];
	for(int i=2;i<=TamPop; i++)
	{
		if(fit[i]>fitnessMelhor)
		{
			indexMelhor = i;
			fitnessMelhor = fit[i];
		}
	}

	for(int i=1;i<=TamPop; i++)
	{
		if(i!=indexMelhor)
		{
			ind[i] = (double) (rand() % maxx);
		}
	}
}

void avalia(void)
{
    float x;
	printf("Generation %d\n",gen);
    for (i=1;i<=TamPop;i++)
    {
        x=ind[i];
		//------- Função doida -------//
		float y = (2*cos(0.039*x) + 5*sin(0.05*x) + 0.5*cos(0.01*x) + 10*sin(0.07*x) + 5*sin(0.1*x) + 5*sin(0.035*x))*10+500;
		//------ Montanha -------//
		//float y = x; 
		//if(x>500)
		//	y=1000-x;
	    fit[i] = y;
		printf("\tFitness %d (%f)= %f\n",i,ind[i],fit[i]);
    }
}

void draw(void) 
{
    //background colour
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
 
    glClear(GL_COLOR_BUFFER_BIT);
 
 	glPointSize(10.0f); //point size
    glLineWidth(5.0f); //line width


  	glPointSize(3.0f); //point size

    //draw Funcao
    glBegin(GL_POINTS);
   		glColor3f(0.4f, 0.4f, 0.0f); //define yellow

		for (int x=0;x<1000;x++)
	    	{
				//------- Função doida -------//
				float y = (2*cos(0.039*x) + 5*sin(0.05*x) + 0.5*cos(0.01*x) + 10*sin(0.07*x) + 5*sin(0.1*x) + 5*sin(0.035*x))*10+500;
				//------ Montanha -------//
				//float y = x; 
				//if(x>500)
				//	y=1000-x;
				glVertex2f((float) x, y);	
	    	}
    glEnd();

	drawTextToScreen();

	// Printa os outros individuos
 	glPointSize(4.0f); //point size
    glBegin(GL_POINTS);
   	glColor3f(0.0f, 1.0f, 0.0f); //define green
    for (i=1;i<=TamPop;i++)
    {                         // nr entre -10 e +10 -> rand() %20 - 10;
        glVertex2f(ind[i], fit[i]);	

    }
    glEnd();

    // Printa Melhor de Todos em Vermelho
 	glPointSize(10.0f); //point size
    glBegin(GL_POINTS);
    	glColor3f(1.0f, 0.0f, 0.0f); //define red
    	glVertex2f(ind[maxi], fit[maxi]); //draw goal
    glEnd();


    //send data to draw
    glFlush();
}

void drawTextToScreen(void)
{
	int yValue = 980;

	// Print generation
	char genQty[30]={'\0'};
	sprintf(genQty, "Generation: %d", gen);// Text
	glColor3f(0.0,1.0,0.0);// Text color
	glRasterPos2f(0,yValue);// Print position
	// Print each char
	for(int j=0;j<30;j++){
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, genQty[j]);
	}
	yValue-=20;

	// Print best ind 
	char bestIndiv[30]={'\0'};
	sprintf(bestIndiv, "Best fitness: %f", maxfit);
	glColor3f(0.0,1.0,0.0);// Text color
	glRasterPos2f(0,yValue);// Print position
	// Print each char
	for(int j=0;j<30;j++){
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, bestIndiv[j]);
	}
	yValue-=20;

	// Print best ind 
	char fitness5[100]={'\0'};
	sprintf(fitness5, "Last fitness: %.3f %.3f %.3f %.3f %.3f", bestInd[0], bestInd[1], bestInd[2], bestInd[3], bestInd[4]);// Text
	glColor3f(0.0,1.0,0.0);// Text color
	glRasterPos2f(0,yValue);// Print position
	// Print each char
	for(int j=0;j<100;j++){
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, fitness5[j]);
	}
	yValue-=20;

	// Print mutation 
	char mutation[30]={'\0'};
	sprintf(mutation, "Mutation: %.3f%%", TaxMut);// Text
	glColor3f(0.0,1.0,0.0);// Text color
	glRasterPos2f(0,yValue);// Print position
	// Print each char
	for(int j=0;j<30;j++){
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, mutation[j]);
	}
	yValue-=20;

	// Print help
	char help[30]={'\0'};
	sprintf(help, "Enter->Next / r->Restart");// Text
	glColor3f(0.0,1.0,0.0);// Text color
	glRasterPos2f(0,yValue);// Print position
	// Print each char
	for(int j=0;j<30;j++){
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, help[j]);
	}
	yValue-=20;

}

void keyboard(unsigned char key, int x, int y)
{
    //ENTER
    if (key == 13){

	// Save data to file
	saveToTxt();
	 	ag();
	 	glutPostRedisplay();
    }
	// r
	if(key == 114){
		gen = 0;
		TaxMut = 60;
		i = 0;
		maxi = 0;
		maxfit = 0;
		initpop();
	 	glutPostRedisplay();
	}
}

void saveToTxt()
{
	char printData[50]={'\0'};

	// Print generation
	sprintf(printData, "%d\t", gen);
	fprintf(file, printData);

	// Print each fitness
	for(int i=1; i<=TamPop; i++)
	{
		sprintf(printData, "%.0f\t", fit[i]);

		fprintf(file, printData);
	}

	// Print max fitness
	sprintf(printData, "%.0f\t", maxfit);
	fprintf(file, printData);

	// Print mean fitness
	float mean = 0;
	for(int i=1; i<=TamPop; i++)
	{
		mean += fit[i];
	}
	mean/=TamPop;
	sprintf(printData, "%.0f", mean);
	fprintf(file, printData);

	fprintf(file, "\n");
}
