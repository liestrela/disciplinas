# Evolutionary Systems Applied to Robotics

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: just one @)
- Department of Computer Sistemas– ICMC - USP
- Group of Embedded and Evolutionary Systems
- Laboratory of Reconfigurable Systems

## Portuguese Version: [here](https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0713-Sistemas-Evolutivos-Aplicados-a-Robotica/README.md)

## 2023 Students – Second Semester
- Attendance list – Please sign the list every class 
- Every code and material developed in classes will be available in this platform. 

## Assessment of student performance
- The assessment of student performance will be consisted of the presentation of a project consisting of the implementation of an evolutionary algorithm applied to any real problem.
- The students will be assigned to groups of 3-5 members for the development of the project.
- The project will be developed in group, but the individual grades will come from the evaluation of student performance in the project.
- EVERY STUDENT WILL HAVE TO BE PRESENT IN THEIR PROJECT PRESENTATION!!

### Project presentation for 2023 – Second semester:
-	Project presentation will be done in a meeting with the professor in the classroom  in the last 3 weeks of the semester: choose a proper day and time in the following table: ==> LINK DOCS: https://docs.google.com/spreadsheets/d/1_ApKDWWo3zTJVUvQOwhrquvahLiqaPpVBGVFXWG8tdY/edit?usp=sharing

- Insert in the table your project title, the names of the students, your USP Id, and a link for the project at github/gitlab 

- The project listed on github/gitlab must contain the software and a Readme file containing the application description and information on how to install all the libraries. 
-  the Project on github/gitlab also must have a link (youtube or GoogleDrive) to a video where the students present their project and describe the evolutionary algorithm. 

## Breno´s OpenGL tutorial - https://github.com/Brenocq/OpenGL-Tutorial

## Other Student code and applications:
- how to use Gnuplot inside your own program - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- EA to evaluate mathematical functions - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- Code of a Multi-layer Perceptron Neural Network - MLP - https://github.com/ncalsolari/Rede-Neural-Generica-3-camadas-

- Lucas´ EAs (Experimentos iniciais com variacao da mutacao e da funcao) - https://github.com/LucasRorisCube/SistemasEvolutivos

# Recorded classes (2021 – second semester)

- Class01 20/08/2021 (14:20h) - https://drive.google.com/file/d/1z0wHTwafRw7nwQTY3KkOe3I3OxQAmEqq/view?usp=sharing

- Class02 27/08/2021 (14:20h) - https://drive.google.com/file/d/1Fs3FRpP9Lc7Xapo_hsEGUy2lUowoZhnA/view?usp=sharing
  - Link pro git dos AGs do Lucas - https://github.com/LucasRorisCube/SistemasEvolutivos

- Class03 03/09/2021 (14:20h) - https://drive.google.com/file/d/1uQqhGOfT46z6nad3q2lEvz49gr2bH9e1/view?usp=sharing

- Class04 10/09/2021 (14:20h) - https://drive.google.com/file/d/1tpfDFrb5jM8fA0mTg3eJcncDAH0btiUf/view?usp=sharing

- Class05 17/09/2021 (14:20h) - https://drive.google.com/file/d/1-ooVRlmOONFZhiUNCR6dh9enuDSUEzkY/view?usp=sharing

- Class06 24/09/2021 (14:20h) - https://drive.google.com/file/d/1Xf0J30Bj8yAWYjvh7oZ-nwebZ2Qwrv2G/view?usp=sharing

- Class07 08/10/2021 (14:20h) - https://drive.google.com/file/d/1N5rFnOVK_dhh3yGfL7NcbMQJMH93C-fJ/view?usp=sharing

- Class08 15/10/2021 (14:20h) - https://drive.google.com/file/d/1UQzIMW8NrJ-ZT8Y07D48_-ig5ITOXcdh/view?usp=sharing

- Class09 22/10/2021 (14:20h) - https://drive.google.com/file/d/1hk7EG5txVCH6fR2i_P7lpe3vyaZT91D-/view?usp=sharing

- Class10 29/10/2021 (14:20h) - https://drive.google.com/file/d/1GlBfaybrS_GHhazd3vBzfk2ZEqGyMNVP/view?usp=sharing

- Class11 12/11/2021 (14:20h) - https://drive.google.com/file/d/1LJnjQ9d0hDZimffYxOUyI-Mlp9NRyk3d/view?usp=sharing

- Class12 19/11/2021 (14:20h) - https://drive.google.com/file/d/1faWAVpnvNxBpCBb0ixFqgkFa0qt31k4Y/view?usp=sharing

- Class13 26/11/2021 (14:20h) - https://drive.google.com/file/d/1w8muGkS9XSUk5HtgEW8dZaGQhkn4OuFU/view?usp=sharing
  
  - NVIDIA’s physics simulation environment for reinforcement learning research - https://developer.nvidia.com/isaac-gym

  - Atta - a robot simulator for 2D and 3D applications (Breno) - https://github.com/brenocq/atta

- Class14 03/12/2021 (14:20h) - https://drive.google.com/file/d/1IydWtxJlGuDXQ3URtGgXkPKWrgr-xe1e/view?usp=sharing

- Class15 10/12/2021 (14:20h) - https://drive.google.com/file/d/1OqYVZPIoZwhr07bjl82ThvHZOwtCTI7W/view?usp=sharing

- Class16 17/12/2021 (14:20h) - https://drive.google.com/file/d/1uB0poci_FSbe4nfAXMvGrurw-VYQMLp7/view?usp=sharing
  - EA solving the Magic cube - https://drive.google.com/file/d/1m1ASuEiF58ZIPgMz9ynGbPV5PQQAcZUd/view?usp=sharing

- Class17 07/01/2022 (14:20h) - https://meet.google.com/btu-crez-aro
